//
//  LoginViewController.swift
//  RevenueCatIOS
//
//  Created by Tarantino O'Connor on 21/08/2020.
//  Copyright © 2020 Tarantino O'Connor. All rights reserved.
//

import Foundation
import UIKit

class LoginViewController : UIViewController {
    
    // References:
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        
        emailField.text = "appdevtwinkl"
        passwordField.text = "Appdev2017"
    }
    
    /// Attempt to login user with given information
    @IBAction func submitClicked(_ sender: UIButton) {
        
        guard let username = emailField.text, let password = passwordField.text else {
            print("User name or password empty!")
            return
        }
        
        // Make login request to Twinkl API, if successful proceed with grace period check.
        TwinklWebServer.loginAndValidateTwinklUser(userName: username, password: password, didComplete: { result in
            
            switch(result)
            {
            case nil: // Thus success
                // Check if within grace period.
                if(TwinklWebServer.checkGracePeriod()) {
                    print("Grace period check passed.")
                    
                    DispatchQueue.main.async {
                      
                        guard let subscriptionViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "SubscriptionViewController") as? ViewController else {
                          fatalError()
                        }
                        self.navigationController?.pushViewController(subscriptionViewController, animated: true)
                   
                    }
                }
                else {
                    //TODO Handle what to do if outside of grace period.
                }
            case .unableToLogin:
                //TODO Add "unable to login" message.
                print("Unable to login")
                
            }
            
            
            
        })
        
        
    }
    
}
