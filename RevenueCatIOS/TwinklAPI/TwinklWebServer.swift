//
//  TwinklWebServer.swift
//  Twinkl
//
//  Created by Josh Robbins (∩｀-´)⊃━☆ﾟ.*･｡ﾟ on 20/03/2018.
//  Copyright © 2018 Twinkl. All rights reserved.
//

import Foundation
import Network
import CryptoSwift

final class TwinklWebServer: NSObject {
    typealias ServerResponseHandler = (APIError?) -> Void
    private static weak var task: URLSessionDataTask?
    private static weak var session =  URLSession.shared
    //fileprivate typealias OptionalErrorHandler = (LernadoError?) -> Void
}

//-------------------------
//MARK:- Login & Validation
//-------------------------

extension TwinklWebServer {
    
    /// Logs In The Twinkl User, Creating An Account On Firebase If Necessary
    /// - Parameters:
    ///   - userName: String
    ///   - password: String
    ///   - didComplete: TwinklAPIError
    static func loginAndValidateTwinklUser(userName: String, password: String, didComplete: @escaping (TwinklAPIError?) -> Void) {
        
        //1. Log The User In Using Their Twink; Email & Password
        loginTwinklUser(userName: userName, password: password) { (userData) in
            
            guard let userData = userData else { didComplete(.unableToLogin)
                //Unable To Sign In
                return
            }
            setAppStatus(demoMode: userData.isActiveSubscriber(email: userName))
            didComplete(nil)
        }
    }
    
    /// Attempts To Log The Twinkl User In & If Succesful Retrieves The Users Subscription Information
    /// - Parameters:
    ///   - userName: String
    ///   - password: String
    ///   - didComplete: (TwinklResponse?)
    /// - Returns: Twinkl Response
    private static func loginTwinklUser(userName: String, password: String, didComplete: @escaping (TwinklResponse?) -> ()) {
        let userAPIData = generateAPIData(userName: userName, password: password)
        let request = urlRequest(function: APIKeys.login, userName: userName, nonceData: userAPIData.nonce, apiKey: userAPIData.apiKey)
        
        runURL(request: request) { (result) in
            
            switch result {
                
            case .success(let data):
                guard let decodedData = try? JSONDecoder().decode(TwinklResponse.self, from: data) else { didComplete(nil)
                    return
                }
               
                didComplete(decodedData)
            case .failure(_):
                didComplete(nil)
            }
        }
    }
    
    /// Sets Up The App For The Given Mode
    /// - Parameter demoMode: Bool
    static func setAppStatus(demoMode: Bool) {
        
    }
    
    
    //---------------------------------
    //MARK:- Grace Period Logic
    //---------------------------------
    
    static func checkGracePeriod() -> Bool {
        
        // Get user defaults.
        let userDefaults = UserDefaults.standard
        
        // Get current date.
        let currentDate = Date()
        
        // Get stored gracePeriod.
        let gracePeriod = userDefaults.object(forKey: "gracePeriod") as? Date
        
        // If exists, increase by three weeks.
        if(gracePeriod != nil) {
            
            // Check if within grace period, if so proceed else "bad ending".
            if let gracePeriodDate = gracePeriod {
                
                if(gracePeriodDate > currentDate) {
                    
                    // Extend grace period.
                    extendGracePeriod(currentDate: currentDate, userDefaults: userDefaults)
                    return true
                } else {
                    //TODO Add logic to reroute user and maybe update grace period if successful?.
                    return false
                }
            }
        }
        else {
            
            // Set grace period to be 3 weeks from now.
            extendGracePeriod(currentDate: currentDate, userDefaults: userDefaults)
            return true
        }
        return false
    }
    
    private static func extendGracePeriod(currentDate: Date, userDefaults: UserDefaults) {
        
        // Retrieve date 3 weeks from now.
        let threeWeeksTimeDate = Calendar.current.date(byAdding: .day, value: 21, to: currentDate)
        
        // Set this in user defaults.
        userDefaults.set(threeWeeksTimeDate, forKey: "gracePeriod")
    }
}

//---------------------------------
//MARK:- User Validation & Creation
//---------------------------------


//-------------------
//MARK:- URL Handling
//-------------------

///HTTP Resonse Codes
fileprivate extension Int {
    static let success = 200
    static let timeout = -1001
    static let failure = 400
}

extension TwinklWebServer {
    
    /// Gnerates The Encoded API Data For The Server
    /// - Parameters:
    ///   - userName: String
    ///   - password: String
    /// - Returns:  APIData
    private static func generateAPIData(userName: String, password: String) -> APIData {
        let nonceData = UUID().uuidString
        let encryptedPassword = (userName.lowercased() + password).md5().sha256()
        let apiKey = (nonceData + encryptedPassword).md5()
        return APIData(nonce: nonceData, apiKey:  apiKey)
    }
    
    /// Gnerates The Specified URL Request
    /// - Parameters:
    ///   - function: String
    ///   - userName: String
    ///   - nonceData: String
    ///   - apiKey: String
    /// - Returns: URLRequest
    private static func urlRequest(function: String, userName: String, nonceData: String, apiKey: String) -> URLRequest {
        var components = URLComponents()
        components.scheme = TwinklAPI.scheme
        components.host = TwinklAPI.host
        components.path = TwinklAPI.apiVersion + function
        
        var request = URLRequest(url: components.url!)
        request.httpMethod = HTTPRequestMethod.get.upperCased
        request.timeoutInterval = 30
        request.addValue(userName, forHTTPHeaderField: AuthorizationHeader.username)
        request.addValue(nonceData, forHTTPHeaderField: AuthorizationHeader.nonce)
        request.addValue(apiKey, forHTTPHeaderField: AuthorizationHeader.apiKey)
        request.addValue(APIKeys.userAgentKey, forHTTPHeaderField: AuthorizationHeader.userAgent)
        return request
    }
    
    /// Runs A Configured URL Session
    ///
    /// - Parameter request: URLRequest
    private static func runURL(request: URLRequest, completion: @escaping (Result<Data, APIError>) -> Void) {
        
        DispatchQueue.global().async {
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                guard let httpResponse = response as? HTTPURLResponse else { completion(.failure(.requestFailed))
                    return
                }
                print(httpResponse.statusCode)
                if httpResponse.statusCode == .success {
                    if let validData = data { completion(.success(validData))
                        return
                    } else { completion(.failure(.invalidData))
                        return
                    }
                } else {
                    if httpResponse.statusCode == .timeout || httpResponse.statusCode == .failure { completion(.failure(.timeOut))
                        return
                    }else { completion(.failure(.responseUnsuccessful))
                        return
                    }
                }
            }
            task.resume()
        }
    }
}
