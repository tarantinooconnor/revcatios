//
//  APIHelpers.swift
//  ARtStudio
//
//  Created by Josh Robbins on 2020/05/29.
//  Copyright © 2020 Twinkl Limited. All rights reserved.
//

import Foundation


//---------------
//MARK: API Setup
//---------------

struct TwinklAPI {
  static let scheme = "https"
  static let host = "api.twinkl.co.uk"
  static let apiVersion = "/v2/"
}

//------------------
//MARK: HTTP Headers
//------------------

struct AuthorizationHeader {
  static let username  = "api_username"
  static let nonce     = "api_nonce"
  static let apiKey    = "api_key"
  static let userAgent = "User-Agent"
}

//-------------------
//MARK: HTTP Requests
//-------------------

enum HTTPRequestMethod: IDDescriptable {
  case get, post
}

//--------------------
//MARK: Login Response
//--------------------

struct TwinklResponse: Codable {
   var twinklid: Int32
   var first_name: String
   var last_name: String
   var subscription: String
  
}

extension TwinklResponse {

  var userID: String { return String(twinklid) }
  
  func isActiveSubscriber(email: String) -> Bool {
    if email.isTwinklEmail() {
      return true
    }
    return !subscription.isEmpty
  }
}

//---------------------------
//MARK: Twinkl User Structure
//---------------------------

struct APIData {
  var nonce: String
  var apiKey: String
}

struct UserCredentials: Codable {
  var id: String
  var lastDateChecked: Date
}

//---------------
//MARK: Constants
//---------------

enum APIKeys {
  static let login = "login-plus"
  static let userAgentKey = "Twinkl-IOS100-0001"
}

//----------------------
//MARK:- Cloud Functions
///---------------------

enum CloudFunctions {
  static let region = "europe-west2"
  static let validate = "existsOnFB"
  static let register = "linkToFB"

  enum Keys: IDDescriptable {
    case uuid
    case email
    case password
  }
}

//--------------------
//MARK: Error Handling
//--------------------

enum TwinklAPIError: Error {
  case unableToLogin
}

enum APIError: Error {
  case requestFailed
  case jsonConversionFailure
  case invalidData
  case responseUnsuccessful
  case jsonParsingFailure
  case timeOut
  
  var localizedDescription: String {
    switch self {
    case .requestFailed: return "Request Failed"
    case .invalidData: return "Invalid Data"
    case .responseUnsuccessful: return "Response Unsuccessful"
    case .jsonParsingFailure: return "JSON Parsing Failure"
    case .jsonConversionFailure: return "JSON Conversion Failure"
    case .timeOut : return "Request did timeout"
    }
  }
}

//-----------------------------
//MARK: Twinkl Staff Validation
//-----------------------------

extension String {
  
  /// Checks To See If The Email Contains Twinkl
  /// - Parameter email: String
  /// - Returns: Bool
  func isTwinklEmail() -> Bool {
    let identifier = "\\b\("twinkl")\\b"
    return self.range(of: identifier, options: .regularExpression) != nil
  }
}
