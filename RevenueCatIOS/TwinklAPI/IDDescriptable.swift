//
//  IDDescriptable.swift
//  Lernado
//
//  Created by Josh Robbins on 2020/01/16.
//  Copyright © 2020 Twinkl Limited. All rights reserved.
//

import UIKit

//---------------------
//MARK:- IDDescriptable
//---------------------

/// Protocol To Simplify The Generation Of Strings In Adoptees
protocol IDDescriptable { }

extension IDDescriptable {
  /// Returns The Adoptee As A String
  var id: String { return String(describing: self) }
  /// Returns The Adoptee As A Capitalized String
  var captialized: String { return id.capitalized }
  /// Returns The Adoptee As An UpperCased String
  var upperCased: String { return id.uppercased() }
  /// Returns The Adoptee's First Character As A CapitalLetter
  var firstUpperCased: String { return id.prefix(1).uppercased() + id.dropFirst() }
  /// Returns The Adoptee As An Extension e.g. .png
  var pathExtension: String { return "." + id }
}
