//
//  ViewController.swift
//  RevenueCatIOS
//
//  Created by Tarantino O'Connor on 19/08/2020.
//  Copyright © 2020 Tarantino O'Connor. All rights reserved.
//

import UIKit
import Purchases

@objc protocol PurchaseDelegate {
    func purchaseCompleted(viewController: ViewController, transaction: SKPaymentTransaction, purchaserInfo: Purchases.PurchaserInfo)
    
    @objc optional func purchaseFailed(viewController: ViewController, purchaserInfo: Purchases.PurchaserInfo?, error: Error, userCancelled: Bool)
    
    @objc optional func purchaseRestored(viewController: ViewController, purchaserInfo: Purchases.PurchaserInfo?, error: Error?)
    
}

class ViewController: UICollectionViewController {
    
    var delegate : PurchaseDelegate?
    
    // Offerings
    var offering : Purchases.Offering? = nil
    let offeringId : String? = "monthly"
    
    // PurchaserInfo
    var purchaserInfo : Purchases.PurchaserInfo? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Set title and obtain current offering.
        navigationItem.title = "Subscriptions"
        navigationItem.setHidesBackButton(true, animated: false)
        retrieveCurrentOffering()
    }
    
    //-----------------
    
    //-----------------
    //MARK:- Collection
    //-----------------
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.offering?.availablePackages.count ?? 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Subscription", for: indexPath) as? SubscriptionCell else {
            fatalError("Unable to dequeue Subscription Cell")
        }
        
        cell.populate(offering: self.offering)
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let package = self.offering?.availablePackages[indexPath.row] else {
            self.showAlert(title: "Error", message: "No available package" ) { (action) in
                self.dismiss(animated: true, completion: nil)
            }
            return
        }
        
        self.showAlert(title: "Selected", message: "Package \(package.product.localizedDescription)" ) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //-----------------
    
    //-----------------
    //MARK:- Purchases
    //-----------------
    
    
    /// Retrive the current offering from revenue cat.
    func retrieveCurrentOffering()
    {
        Purchases.shared.offerings { (offerings, error)  in
            
            // If an error occurs.
            if error != nil {
                self.showAlert(title: "Error", message: "Unable to fetch offerings.") { (action) in
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
            // Try get current offer via offering id otherwise just get the stated current offer.
            if let offeringId = self.offeringId {
                self.offering = offerings?.offering(identifier: offeringId)
            } else {
                self.offering = offerings?.current
            }
            
            // If offering has not been set, no offerings found.
            if self.offering == nil {
                self.showAlert(title: "Error", message: "No offerings found.") { (action) in
                    self.dismiss(animated: true, completion: nil)
                }
            }
            else {
                
                // Reload data of collection view, so data from offering can be displayed.
                self.collectionView.reloadData()
                self.retrievePurchaserInfo()
            }
        }
        
    }
    
    /// Get Purchaser Info
    func retrievePurchaserInfo(){
        
        Purchases.shared.purchaserInfo{(purchaseInfo, error) in
            
            // If an error occurs.
            if error != nil {
                self.showAlert(title: "Error", message: "Unable to fetch purchaser info.") { (action) in
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
            // Store Purchase info.
            if(purchaseInfo != nil)
            {
                self.purchaserInfo = purchaseInfo
                print(purchaseInfo?.activeSubscriptions)
                print(purchaseInfo?.entitlements)
            }
        }
    }
    
    func retrieveActiveSubscritpions() -> Set<String>?
    {
        return self.purchaserInfo?.activeSubscriptions
    }
    
    func retrieveEntitlements() -> Purchases.EntitlementInfos?
    {
        return self.purchaserInfo?.entitlements
    }
    
    func restorePurchases(){
        
        Purchases.shared.restoreTransactions { (info, error) in
            
            if let purchaseRestoredHandler = self.delegate?.purchaseRestored {
                purchaseRestoredHandler(self, info, error)
            } else {
                if let error = error {
                    self.showAlert(title: "Restore Unsuccessful", message: "No prior purchases found for your account.")
                } else {
                    
                    if let purchaserInfo = info {
                        if purchaserInfo.entitlements.active.isEmpty {
                          self.showAlert(title: "Restore Unsuccessful", message: "No prior purchases found for your account.")
                        } else {
                          self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    /// Show alert to user with information.
    private func showAlert(title: String?, message: String?, handler: ((UIAlertAction) -> Void)? = nil) {
        // Create an alert.
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // Add 'ok' button as an action.
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: handler))
        
        // Show to user.
        self.present(alert, animated: true, completion: nil)
    }
}

