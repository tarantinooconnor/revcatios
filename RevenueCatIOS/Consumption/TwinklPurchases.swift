//
//  TwinklPurchases.swift
//  RevenueCatIOS
//
//  Created by Tarantino O'Connor on 19/08/2020.
//  Copyright © 2020 Tarantino O'Connor. All rights reserved.
//

import Foundation
import Purchases

class TwinklPurchases {
    
    var offering : Purchases.Offering? = nil
    let offeringId : String? = "monthly"
    
    func currentOffering() -> Purchases.Offering?
    {
     
    
        
         Purchases.shared.offerings { (offerings, error)  in
                   
                   if error != nil {
                     print("Unable to fetch offerings")
                   }
                   if let offeringId = self.offeringId {
                       self.offering = offerings?.offering(identifier: offeringId)
                   } else {
                       self.offering = offerings?.current
                 
                   }
                
                   if self.offering == nil {
                     print("No offerings found")
                       }
                   else{
                    print("Offering not nil")
                    print("!BOO \(self.offering?.monthly?.localizedPriceString)")
                   
            }
                   }
      return self.offering
               
               }
  

}

