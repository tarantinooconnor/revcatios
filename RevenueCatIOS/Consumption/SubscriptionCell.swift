//
//  SubscriptionCell.swift
//  RevenueCatIOS
//
//  Created by Tarantino O'Connor on 19/08/2020.
//  Copyright © 2020 Tarantino O'Connor. All rights reserved.
//

import UIKit
import Purchases

class SubscriptionCell: UICollectionViewCell {
    
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    public func populate(offering:Purchases.Offering?)
    {
        durationLabel.text = offering?.monthly?.product.localizedDescription
        priceLabel.text = offering?.monthly?.localizedPriceString
    }
}
